import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import ItemShoe from "./ItemShoe";
import ModalCart from "./ModalCart";
import ModalDetailShoe from "./ModalDetailShoe";

export default class ShoeShop extends Component {
  state = {
    dataShoe: dataShoe,
    dataCart: [],
    detailShoe: {},
  };
  handleChangeDetailShoe = (itemShoe) => {
    this.setState({ detailShoe: itemShoe });
  };
  handleAddShoeToCart = (itemShoe) => {
    let indexShoeCheck = this.state.dataCart.findIndex((item) => {
      return item.id === itemShoe.id;
    });
    let updateCart = [...this.state.dataCart];
    if (indexShoeCheck === -1) {
      updateCart.push({ ...itemShoe, soLuong: 1 });
    } else {
      updateCart[indexShoeCheck].soLuong += 1;
    }
    this.setState({ dataCart: updateCart });
  };
  handlechangeQuantity = (indexShoe, upDown) => {
    let updateCart = [...this.state.dataCart];
    if (upDown) {
      updateCart[indexShoe].soLuong += 1;
    } else {
      if (updateCart[indexShoe].soLuong > 1) {
        updateCart[indexShoe].soLuong -= 1;
      }
    }
    this.setState({ dataCart: updateCart });
  };
  handleDeleteShoeInCart = (index) => {
    let updateCart = [...this.state.dataCart];
    updateCart.splice(index, 1);
    this.setState({ dataCart: updateCart });
  };
  handleDeletedataCart = () => {
    this.setState({ dataCart: [] });
  };
  renderListShoe = () => {
    return this.state.dataShoe.map((itemShoe, index) => {
      return (
        <ItemShoe
          itemShoe={itemShoe}
          key={index}
          handleChangeDetailShoe={this.handleChangeDetailShoe}
          handleAddShoeToCart={this.handleAddShoeToCart}
        />
      );
    });
  };
  render() {
    let totalQuantity = this.state.dataCart.reduce((result, itemShoe) => {
      return (result += itemShoe.soLuong);
    }, 0);
    return (
      <div className="container p-3">
        {/* Cart Header */}
        <div className="row bg-light p-3 align-items-center">
          <h1 className="col-6 text-start">TÂN TÂN SHOE STORT</h1>
          <div className="offset-4 col-2 text-end">
            <button
              className="btn btn-success"
              data-bs-toggle="modal"
              data-bs-target="#showModalCart"
            >
              <i className="fa fa-cart-plus" />({totalQuantity})
            </button>
          </div>
          <ModalCart
            dataCart={this.state.dataCart}
            handlechangeQuantity={this.handlechangeQuantity}
            handleDeleteShoeInCart={this.handleDeleteShoeInCart}
            handleDeletedataCart={this.handleDeletedataCart}
          />
        </div>
        {/* render dataShoe */}
        <div className="row">{this.renderListShoe()}</div>
        <ModalDetailShoe
          detailShoe={this.state.detailShoe}
          handleAddShoeToCart={this.handleAddShoeToCart}
        />
      </div>
    );
  }
}
