import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { id, image, name, price, description } = this.props.detailShoe;
    return (
      <div className="row mt-2 alert-secondary p-3 text-start">
        <img src={image} alt="" className="col-3" />
        <div className="col-9">
          <p>ID: {id}</p>
          <p>Name: {name}</p>
          <p>Price: $ {price}</p>
          <p>Description: {description}</p>
        </div>
      </div>
    );
  }
}
