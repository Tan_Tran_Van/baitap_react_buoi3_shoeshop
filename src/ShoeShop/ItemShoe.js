import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price, description } = this.props.itemShoe;
    return (
      <div className="col-4 p-2">
        <div className="card text-start">
          <img className="card-img-top" src={image} alt={name} />
          <div className="card-body alert-secondary bg-dark bg-opacity-25 py-1">
            <h4 className="card-title">{name}</h4>
            <h3 className="card-text">$ {price} USD</h3>
            <p className="card-text">
              {description.length > 60
                ? description.substr(0, 60) + "..."
                : description}
            </p>
            <div className="row justify-content-around">
              <button
                className="btn btn-warning col-5"
                data-bs-toggle="modal"
                data-bs-target="#showModalDetailShoe"
                onClick={() => {
                  this.props.handleChangeDetailShoe(this.props.itemShoe);
                }}
              >
                Xem Chi Tiết
              </button>
              <button
                className=" btn btn-success col-5"
                onClick={() => {
                  this.props.handleAddShoeToCart(this.props.itemShoe);
                }}
              >
                Thêm Vào Giỏ Hàng
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
