import React, { Component } from "react";

export default class Cart extends Component {
  renderDataCart = () => {
    return this.props.dataCart.map((shoe, index) => {
      return (
        <tr key={index}>
          <td>{shoe.id}</td>
          <td>
            <img style={{ width: "50px" }} src={shoe.image} alt="" />
          </td>
          <td>{shoe.name}</td>
          <td>
            <button
              className="btn font-weight-bold"
              onClick={() => {
                this.props.handlechangeQuantity(index, false);
              }}
            >
              -
            </button>
            <span className="px-2">{shoe.soLuong}</span>

            <button
              className="btn font-weight-bold"
              onClick={() => {
                this.props.handlechangeQuantity(index, true);
              }}
            >
              +
            </button>
          </td>
          <td>$ {shoe.price.toLocaleString()}</td>
          <td>$ {(shoe.soLuong * shoe.price).toLocaleString()}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDeleteShoeInCart(index);
              }}
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    let tongTien = this.props.dataCart.reduce((tongTien, item, index) => {
      return (tongTien += item.soLuong * item.price);
    }, 0);
    return (
      <div>
        <table className="table text-center">
          <thead>
            <tr>
              <td>ID</td>
              <td>Hình Ảnh</td>
              <td>Tên Sản Phẩm</td>
              <td>Số Lượng</td>
              <td>Đơn giá</td>
              <td>Thành Tiền</td>
              <td></td>
            </tr>
          </thead>
          <tbody>{this.renderDataCart()}</tbody>
          <tfoot>
            <tr>
              <td colSpan="5" className="text-end fs-5 fw-bold">
                Tổng Tiền:
              </td>
              <td className="text-center fs-5 fw-bold">
                $ {tongTien.toLocaleString()}
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
