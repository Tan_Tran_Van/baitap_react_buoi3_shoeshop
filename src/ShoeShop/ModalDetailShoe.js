import React, { Component } from "react";
import DetailShoe from "./DetailShoe";

export default class ModalDetailShoe extends Component {
  render() {
    return (
      <div>
        <div
          className="modal fade"
          id="showModalDetailShoe"
          tabIndex={-1}
          aria-labelledby="showModalDetailShoeLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" style={{ minWidth: "1000px" }}>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="showModalDetailShoeLabel">
                  THÔNG TIN SẢN PHẨM
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <div className="modal-body">
                <DetailShoe detailShoe={this.props.detailShoe} />
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-bs-dismiss="modal"
                >
                  CLOSE
                </button>
                <button
                  className="btn btn-success"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    this.props.handleAddShoeToCart(this.props.detailShoe);
                  }}
                >
                  Add to Cart
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
