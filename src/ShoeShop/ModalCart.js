import React, { Component } from "react";
import Cart from "./Cart";

export default class ModalCart extends Component {
  render() {
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="showModalCart"
          tabIndex={-1}
          aria-labelledby="showModalCartLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" style={{ minWidth: "900px" }}>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="showModalCartLabel">
                  THÔNG TIN GIỎ HÀNG
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <div className="modal-body">
                <Cart
                  dataCart={this.props.dataCart}
                  handlechangeQuantity={this.props.handlechangeQuantity}
                  handleDeleteShoeInCart={this.props.handleDeleteShoeInCart}
                />
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    this.props.handleDeletedataCart();
                  }}
                >
                  Xoá sản Phẩm
                </button>
                <button
                  type="button"
                  className="btn btn-success"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    this.props.handleDeletedataCart();
                  }}
                >
                  Thanh Toán
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
